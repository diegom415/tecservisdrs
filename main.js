window.addEventListener('load', function () {
    console.log('el contenido ha cargado!');

    var imagenes = [];

    imagenes[0] = 'assets/img/imagenes1.jpg';
    imagenes[1] = 'assets/img/imagenes2.jpg';
    imagenes[2] = 'assets/img/imagenes3.jpg';
    imagenes[3] = 'assets/img/imagenes4.jpg';
    imagenes[4] = 'assets/img/imagenes5.jpg';

    var indiceImagenes = 0;
    var tiempo = 3000;

    function cambiarImagenes() {

        document.slider.src = imagenes[indiceImagenes];

        if (indiceImagenes < 4) {
            indiceImagenes++;
        } else {
            indiceImagenes = 0;
        }
    }

    setInterval(cambiarImagenes, tiempo);

});